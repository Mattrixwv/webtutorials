//This is an example of manual csrf token inclusion using thymeleaf
//This retrieves and sets up all of the data for the table
function setAjaxCsrfToken(){
	console.log("setAjaxCsrfToken()");
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");

	$(document).ajaxSend(function(e, xhr, options){
		xhr.setRequestHeader(header, token);
	});
}