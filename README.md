# Web Tutorials

This repository was created for my own reference, to keep track of what I've done and remember how to do some of the more complicated things in these languages.

# Here is some sample markdown!

# Testing a **header**
## And a smaller header
### Yet smaller
#### How many headers are there?
##### Just one more
###### Wow, why is this one so small?

_Combined **bold** and italic_ text

~~Strikethrough~~

Testing an *inline* code block `ptr = nullptr`

Testing a larger code block
```C++
int num = 6;
```

Testing a list (Doesn't seem to work on all viewers):
1. Hello
1. World
- Second
- Try

Task List (Doesn't seem to work on all viewers):
  - [x] Completed
  - [ ] Uncompleted


Table (Doesn't seem to work on all viewers):
First Column | Second column
------------ | -------------
Test data 1 | Test data 2
Test data 3 | Test data 4


And finally, a quote
>I see nothing, I hear nothing, I know NOTHING! -
>Sgt. Hanz Schultz

Here is a link [to my Bitbucket](https://bitbucket.org/Mattrixwv)

And an image:

![Octocat](https://octodex.github.com/images/original.png)
